import os
import subprocess

module_name = '{{ cookiecutter.python_slug }}'

welcome = f"""

Hey there!

You have a brand spanking new python project with some basic
CI/CD and pre-commit hooks to keep code clean.

Before you get started, please make sure you install the precommit
hooks - the sooner you start using them the more consistent your
project will be!

cd {module_name}
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
pre-commit install

"""

print(welcome)
