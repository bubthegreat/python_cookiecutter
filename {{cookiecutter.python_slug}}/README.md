# {{cookiecutter.python_slug}}

This is a cookiecutter repo made from https://gitlab.com/bubthegreat/python_cookiecutter.  You can use it by running:

```bash
cookiecutter https://gitlab.com/bubthegreat/python_cookiecutter
```

## Installation

```bash
pip install {{cookiecutter.python_slug}}
```

## CI/CD

This CI/CD assumes that there are two variables set for the upload of your pypy packages:

`TWINE_PASSWORD`
`TWINE_USERNAME`

For the initial CI/CD, this is set to upload to testpypy rather than the actual pypy server.  If tags are used, it will push to production pypy.  This should let you differentiate between testing your package upload and actually publishing it to the pypy index.
