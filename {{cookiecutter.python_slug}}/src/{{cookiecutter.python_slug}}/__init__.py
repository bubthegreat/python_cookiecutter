"""Configs for the library."""

import os
import logging
import logging.config

LIBRARY_NAME = os.path.basename(os.path.dirname(__file__))
LOG_PATH = os.path.expanduser(f"~/.{LIBRARY_NAME}")
if not os.path.exists(LOG_PATH):
    os.makedirs(LOG_PATH)

# Formats for logging
LOGGING_DATE_FORMAT = "%m/%d/%Y %I:%M:%S %p"
NORMAL_LOGGING_FORMAT = "%(asctime)s %(levelname)-8s [%(name)s] %(message)s"
VERBOSE_LOGGING_FORMAT = (
    "%(asctime)s %(levelname)-8s [%(name)s.%(funcName)s:%(lineno)d] %(message)s"
)

# Configurable logging settings
CONSOLE_LOGGING_LEVEL = os.environ.get(f"{LIBRARY_NAME.upper()}_CONSOLE_LOGGING_LEVEL", logging.INFO)
FILE_LOGGING_LEVEL = os.environ.get(f"{LIBRARY_NAME.upper()}_FILE_LOGGING_LEVEL", logging.INFO)
LOGGING_FORMATTER = os.environ.get(
    f"{LIBRARY_NAME.upper()}_LOGGING_VERBOSITY", "normal"
)

# General logging config here.
LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "verbose": {"datefmt": LOGGING_DATE_FORMAT, "format": VERBOSE_LOGGING_FORMAT,},
        "normal": {"datefmt": LOGGING_DATE_FORMAT, "format": NORMAL_LOGGING_FORMAT,},
    },
    "handlers": {
        "console": {
            "level": CONSOLE_LOGGING_LEVEL,
            "class": "logging.StreamHandler",
            "formatter": LOGGING_FORMATTER,
        },
        "file_handler": {
            "level": FILE_LOGGING_LEVEL,
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": LOGGING_FORMATTER,
            "filename": f"{LOG_PATH}/rhs.log",
            "mode": "a",
            "maxBytes": 1048576,
            "backupCount": 10
        },
        "null": {
            "level": CONSOLE_LOGGING_LEVEL,
            "class": "logging.NullHandler",
            "formatter": LOGGING_FORMATTER,
        },
    },
    "loggers": {LIBRARY_NAME: {"handlers": ["console", "file_handler"], "level": FILE_LOGGING_LEVEL,},},
}

# Configure the logging, and then log a message after import of the library.
logging.config.dictConfig(LOGGING_CONFIG)
LOGGER = logging.getLogger(LIBRARY_NAME)
LOGGER.info("Loaded %s library.", LIBRARY_NAME)
