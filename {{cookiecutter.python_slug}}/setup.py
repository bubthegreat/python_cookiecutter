"""Setup configuration and dependencies for {{cookiecutter.python_slug}}."""

from setuptools import find_packages
from setuptools import setup


with open("requirements.txt") as req_file:
    REQUIREMENTS = req_file.readlines()

COMMANDS = [
    "example_command={{cookiecutter.python_slug}}.example:main",
]

setup(
    name="{{cookiecutter.python_slug}}",
    version="0.0.0.alpha0",
    author="{{cookiecutter.author}}",
    author_email="{{cookiecutter.author_email}}",
    url="{{cookiecutter.url}}",
    include_package_data=True,
    description="{{cookiecutter.project_description}}",
    packages=find_packages('src'),
    package_dir={
        '': 'src',
    },
    python_requires=">=3.6.6",
    entry_points={"console_scripts": COMMANDS},
    install_requires=REQUIREMENTS,
)
