"""General importability tests for {{cookiecutter.python_slug}}."""

def test_import() -> None:
    """Test that the base library can be imported."""
    import {{cookiecutter.python_slug}}  # pylint: disable=import-outside-toplevel,unused-import
