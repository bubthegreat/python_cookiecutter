Remove-Item -LiteralPath "my_python_project" -Force -Recurse
cookiecutter --no-input .
Set-Location .\my_python_project\
python .\setup.py sdist
pip install .\dist\my_python_project-0.0.0a0.tar.gz
pytest -n auto --cov-report term-missing --cov-config=.coveragerc --cov=my_python_project tests
pylint --rcfile=.pylintrc src/
mypy --config-file mypy.ini src
example_command mike
example_command -p mike
Set-Location ../
Remove-Item -LiteralPath "my_python_project" -Force -Recurse
